///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   28_04_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty() {                             //Added from CatEmpire

   if( topCat == nullptr )
      return true;

   else
      return false;
}

void CatEmpire::addCat( Cat* newCat) {


   if( topCat == nullptr ){
      topCat = newCat;
      return;
   }
   else
      CatEmpire::addCat( topCat, newCat );

}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){

   if( atCat -> name > newCat -> name ) {
      if( atCat -> left == nullptr ) {
         atCat -> left = newCat;
      } else {
         CatEmpire::addCat( atCat-> left, newCat );
      }
   }else{
      if( atCat -> right == nullptr) {
         atCat -> right = newCat;
      } else {
         CatEmpire::addCat( atCat-> right, newCat );
      }
   }
}

void CatEmpire::dfsInorderReverse( Cat* atCat , int depth ) const{

   if( atCat == nullptr )
      return;


   depth++;

   CatEmpire::dfsInorderReverse( atCat -> right, depth );


   if( atCat -> left !=  nullptr && atCat -> right != nullptr ){
      cout << string( 6 * (depth-1), ' ')  << atCat -> name << "<" <<  endl;
   }else{

      
      if( atCat -> left != nullptr){
         cout << string( 6 * (depth-1), ' ')  << atCat -> name << "\\" <<  endl;
      }else if( atCat -> right != nullptr){
         cout << string( 6 * (depth-1), ' ')  << atCat -> name << "/" <<  endl;
      }else{
         cout << string( 6 * (depth-1), ' ')  << atCat -> name <<  endl;
      }
   }


   CatEmpire::dfsInorderReverse( atCat -> left, depth );


}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   CatEmpire::dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorder( Cat* atCat ) const{

   if( atCat == nullptr )
      return;

   CatEmpire::dfsInorder( atCat->left );

   cout << atCat -> name <<  endl;

   CatEmpire::dfsInorder( atCat->right );


}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}


   CatEmpire::dfsInorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const{

   if( atCat == nullptr )
      return;

   if( atCat -> right != nullptr && atCat -> left != nullptr ){
      
      cout << atCat -> name << " begat " << atCat -> left -> name << " and " << atCat -> right -> name << endl;
   
   }else{

   if( atCat -> left == nullptr && atCat -> right != nullptr )
      cout << atCat -> name << " begat " << atCat -> right -> name << endl;
   
   if( atCat -> right == nullptr && atCat -> left != nullptr )
      cout << atCat -> name << " begat " << atCat -> left -> name << endl;
   
   }
   
   CatEmpire::dfsPreorder( atCat -> left );

   CatEmpire::dfsPreorder( atCat -> right );

}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

dfsPreorder( topCat );
}

void CatEmpire::bfsTraverse( Cat* atCat ) const{

   int Generation = 1;

   queue<Cat*> catQueue;
   
   catQueue.push(atCat);

   while(!catQueue.empty()){

      CatEmpire::getEnglishSuffix(Generation);

      long unsigned int CatsPerGen = catQueue.size();
      
//      cout << endl << "Amount of Cats Per Generation " << CatsPerGen << endl;

      while( CatsPerGen != 0 ){
      
         Cat* Held = catQueue.front();
   
         cout << Held->name << " ";
   
         catQueue.pop();
            
         if( Held -> left != nullptr )   
            catQueue.push(Held->left);
      
         if( Held -> right != nullptr )
            catQueue.push(Held->right);

         CatsPerGen--;

      }
      
      Generation++;

      cout << endl;
   }
}


void CatEmpire::catGenerations() const{
   if( topCat == nullptr ) {
      cout << "No cats!" << endl;
      return;
   }

bfsTraverse( topCat );
}

void CatEmpire::getEnglishSuffix( int n ) const{

   if( n <= 0 )
      return;

   if( (4 <= n && n <= 19) || (n % 10 == 0) )
      cout << n << "th Generation" << endl;

   else if( (n-1) % 10 == 0 )
      cout << n << "st Generation" << endl;

   else if( (n-2) % 10 == 0 )
      cout << n << "nd Generation" << endl;

   else if( (n-3) % 10 == 0 )
      cout << n << "rd Generation" << endl;

   else
      cout << n << "th Generation" << endl;


}
